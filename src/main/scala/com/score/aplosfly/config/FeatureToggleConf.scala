package com.score.aplosfly.config

import com.typesafe.config.ConfigFactory

import scala.util.Try

trait FeatureToggleConf {
  // config object
  val featureToggleCong = ConfigFactory.load("feature-toggle.conf")

  lazy val useMockFabric = Try(featureToggleCong.getBoolean("feature-toggle.use-mock-fabric")).getOrElse(false)
}