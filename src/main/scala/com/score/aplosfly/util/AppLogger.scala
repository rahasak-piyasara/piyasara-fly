package com.score.aplosfly.util

import java.io.{PrintWriter, StringWriter}

import org.slf4j

trait AppLogger {

  val logger = slf4j.LoggerFactory.getLogger(this.getClass)

  def logError(throwable: Throwable): Unit = {
    val writer = new StringWriter
    throwable.printStackTrace(new PrintWriter(writer))
    logger.error(writer.toString)
  }
}
