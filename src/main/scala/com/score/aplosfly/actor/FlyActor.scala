package com.score.aplosfly.actor

import java.util.{Date, UUID}

import akka.actor.{Actor, Props}
import com.score.aplosfly.cassandra.{CassandraStore, Trans}
import com.score.aplosfly.config.AppConf
import com.score.aplosfly.util.AppLogger

import scala.concurrent.duration._
import scala.util.Random


object FlyActor {

  case class Fly()

  val flights = Array("UL130", "UK450", "AR300", "LT891", "TR617", "MH774", "3K761", "VJC811", "9W4591", "3K761", "PR737", "UL140", "TR763")
  val froml = Array("LK", "SG", "ORO", "HLK", "WSSS ATFMU", "VTBS FDMC", "VTBS ATFMU", "WSSS FDMC", "WMKK ATFMU")
  val tol = Array("JK", "JFK", "LHR", "GH", "RPLL FDMC", "WIII ATFMU", "VTBS ARO", "RPLL ATFMU", "WIII FDMC")
  val sl = Array("In progress", "Done")

  def props() = Props(new FlyActor)

}

class FlyActor extends Actor with AppConf with AppLogger {

  import FlyActor._
  import context._

  override def receive: Receive = {
    case Fly =>
      logger.debug(s"fly.. ")

      val id = UUID.randomUUID().toString
      val flightNo = Random.shuffle(flights.toList).head
      val from = Random.shuffle(froml.toList).head
      val to = Random.shuffle(tol.toList).head
      val s = Random.shuffle(sl.toList).head
      CassandraStore.createTrans(Trans(id, "execersg", "FlightActor", "{\"message\": \"flight\"}", id, flightNo, new Date, s, from, to, ""))

      context.system.scheduler.scheduleOnce(flyInterval.seconds, self, Fly)
  }
}
