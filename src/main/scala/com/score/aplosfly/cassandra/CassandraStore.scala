package com.score.aplosfly.cassandra

import java.util.Date

import com.score.aplosfly.util.AppLogger

case class Account(address: String, balance: Int, pubKey: String, timestamp: Date = new Date)

case class Trans(id: String, execer: String, actor: String, message: String,
                 flightId: String, flightNo: String, flightTime: Date, flightStatus: String, flightFrom: String,
                 flightTo: String, flightEvent: String,
                 timestamp: Date = new Date)

object CassandraStore extends CassandraCluster with AppLogger {

  lazy val dsps = session.prepare("SELECT id FROM mystiko.trans where execer = ? AND id = ? LIMIT 1")
  lazy val ctps = session.prepare("INSERT INTO mystiko.trans(id, execer, actor, message, " +
    "flightId, flightNo, flightTime, flightStatus, flightFrom, flightTo, flightEvent, timestamp) " +
    "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")

  def isDoubleSpend(execer: String, id: String): Boolean = {
    // check weather given trans with id exists
    val row = session.execute(dsps.bind(execer, id)).one()
    row != null
  }

  def createTrans(trans: Trans) = {
    // create trans
    session.execute(ctps.bind(trans.id, trans.execer, trans.actor, trans.message,
      trans.flightId, trans.flightNo, trans.flightTime, trans.flightStatus, trans.flightFrom, trans.flightTo, trans.flightEvent,
      trans.timestamp))
  }
}

