package com.score.aplosfly

import akka.actor.ActorSystem
import com.score.aplosfly.actor.FlyActor
import com.score.aplosfly.actor.FlyActor.Fly
import com.score.aplosfly.config.AppConf
import com.score.aplosfly.util.LoggerFactory

object Main extends App with AppConf {

  // setup logging
  LoggerFactory.init()

  // init aplos
  implicit val system = ActorSystem.create("mystiko")
  system.actorOf(FlyActor.props(), name = "FlyActor") ! Fly

}
